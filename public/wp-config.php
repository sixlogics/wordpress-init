<?php
/**
 * Set root path.
 */
$rootPath = realpath(__DIR__ . '/..');

/**
 * Include the Composer autoloader.
 */
include $rootPath . '/vendor/autoload.php';

/**
 * Set site URL.
 */
$server_url = getenv('SITE_URL');

/**
 * Define environment.
 */
define('APP_ENV', getenv('APP_ENV'));

/**
 * Set database details.
 */
// WRITER ENDPOINT
define('DB_NAME',            getenv('DB_NAME'));
define('DB_USER',            getenv('DB_USER'));
define('DB_PASSWORD',        getenv('DB_PASSWORD'));
define('DB_HOST',            getenv('DB_HOST'));
// READER ENDPOINT
define('DB_NAME_READER',     getenv('DB_NAME_READER'));
define('DB_USER_READER',     getenv('DB_USER_READER'));
define('DB_PASSWORD_READER', getenv('DB_PASSWORD_READER'));
define('DB_HOST_READER',     getenv('DB_HOST_READER'));

/**
 * Set debug mode.
 */
define('WP_DEBUG', getenv('WP_DEBUG') === 'true' ? true : false);

/**
 * Enable W3 Total Cache.
 */

/**
 * SSL.
 */
define('FORCE_SSL_ADMIN', getenv('WP_FORCE_SSL') === 'true' ? true : false);
define('FORCE_SSL_LOGIN', getenv('WP_FORCE_SSL') === 'true' ? true : false);

if ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' )
{
    $_SERVER['HTTPS'] = 'on';
}

/**
 * Set custom paths.
 * These are required because WordPress is installed in a subdirectory.
 */
define('WP_CONTENT_URL', $server_url . 'content');
define('WP_SITEURL',     $server_url . 'wordpress');
define('WP_HOME',        $server_url);
define('WP_CONTENT_DIR', __DIR__ . '/content');

/**
 * Usual Wordpress stuff - don't overide the ones you have already.
 */
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

/**
 * Authentication unique keys and salts.
 *
 * @link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service
 */
define('AUTH_KEY',         getenv('AUTH_KEY'));
define('SECURE_AUTH_KEY',  getenv('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY',    getenv('LOGGED_IN_KEY'));
define('NONCE_KEY',        getenv('NONCE_KEY'));
define('AUTH_SALT',        getenv('AUTH_SALT'));
define('SECURE_AUTH_SALT', getenv('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT',   getenv('LOGGED_IN_SALT'));
define('NONCE_SALT',       getenv('NONCE_SALT'));

/**
 * WordPress database table prefix.
 * Use something other than `wp_` for security.
 */
$table_prefix = getenv('DB_PREFIX');

define('DISABLE_WP_CRON', getenv('DISABLE_WP_CRON'));
define('WP_POST_REVISIONS', getenv('WP_POST_REVISIONS'));

/**
 * Absolute path to the WordPress directory.
 */
if (!defined('ABSPATH'))
{
    define('ABSPATH', dirname(__FILE__) . '/');
}

require_once(ABSPATH . 'wp-settings.php');

valkyrie();
