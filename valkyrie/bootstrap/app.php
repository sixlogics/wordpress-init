<?php

/**
 * Build root path.
 */
$rootPath = realpath(__DIR__.'/../..');

/*
 * Fetch .env
 */
if (file_exists($rootPath.'/.env')) {
    Dotenv::load($rootPath);
}

/*
 * Check if getenv is not working
 */
if (getenv('APP_ENV') == false) {
    die('Check your settings, you may need to add a .env file');
}

/***
 * Mailer
 */
function wp_mail($to, $subject, $message, $headers = '', $attachments = [])
{
    return Valkyrie\Mailer::boot($to, $subject, $message, $headers, $attachments);
}

function valkyrie()
{

    if ( function_exists('add_action') ) {
        /*
        * Schedule Route
        */
        add_action('parse_request', function ($wp) {
            if (isset($_GET['valkyrie'])) {
                $name = $_GET['valkyrie'];

                if ($name == 'scheduler' &&
                    getenv('SCHEDULER_ROUTE') == 'true' &&
                    getenv('SCHEDULER_TOKEN') == $_GET['token']
                ) {
                    include 'scheduler.php';
                }

                die(0);
            }
        });
    }

    if ( function_exists('do_action') ) {
        do_action('valkyrie');
    }
}
